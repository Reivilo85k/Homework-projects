import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

public class School implements ClassManagementInterface {
    private List<Student> studentList;
    private List<Trainer> trainerList;
    private List<Group> groupList; //{Group group1; Group group2; Group group3; Group group4} ;
    Scanner input = new Scanner(System.in);

    public School(List<Student> studentList, List<Trainer> trainerList, List<Group> groupList) {
        this.studentList = studentList;
        this.trainerList = trainerList;
        this.groupList = groupList;
    }

    public School() {
    }

    public List<Student> getStudentList() {
        return studentList;
    }

    public void setStudentList(List<Student> studentList) {
        this.studentList = studentList;
    }

    public List<Trainer> getTrainerList() {
        return trainerList;
    }

    public void setTrainerList(List<Trainer> trainersList) {
        this.trainerList = trainersList;
    }

    public List<Group> getGroupList() {
        return groupList;
    }

    public void setGroupList(List<Group> groupList) {
        this.groupList = groupList;
    }

    @Override
    public List<Student> createStudentList() {
        List<Student> studentList = new ArrayList<Student>(15);
        int index = 0;

        //this will loop until 15 students are created automatically and added to the ArrayList
       while (index < 15){
        Student student = new Student();
        studentList.add(index, student.createPerson());
        index++;
        }
        return studentList;
    }

    @Override
    public List<Trainer> createTrainerList() {
        List<Trainer> trainerList = new ArrayList<Trainer>(3);
        int index = 0;

        //this will loop until 3 students are created automatically and added to the ArrayList
        while (index < 3){
            Trainer trainer = new Trainer();
            trainerList.add(index, trainer.createPerson());
            index++;
        }
        return trainerList;
    }

    @Override
    public List<Group> createGroupList(List<Student> studentList, List<Trainer> trainerList) {
        int studentIndex = 0;
        int trainerIndex = 0;
        int groupListIndex = 0;
        int numberOfGroups = 0;

        List<Group> groupList = new ArrayList<Group>();

        //This will loop until 15 students (created earlier) have been input in groups
        while (studentIndex < 15){
            //the class parameter numberOfGroups is used to name the group
            String groupName = "Group"+numberOfGroups;
            Group newGroup = new Group(trainerList.get(trainerIndex),
                    studentList.get(studentIndex),
                    studentList.get(studentIndex+1),
                    studentList.get(studentIndex+2), groupName);
            numberOfGroups++;
            groupList.add(groupListIndex, newGroup);
            //The automatic creator makes groups of 3 people by default
            studentIndex += 3;
            groupListIndex++;
            //this makes it certain each trainer has a group given to him,
            if (trainerIndex<2){
                trainerIndex++;
                //after that the trainer is chosen randomly
            }else trainerIndex = ThreadLocalRandom.current().nextInt(0, 2+1);
        }
        return groupList;
    }

    @Override
    public void displayStudentsGroupedByTrainers(List<Group> groupList){
        //I use the trainer as a kay and the students as values to display all students by trainer
        //The HashMap is populated in method createSchoolMap
        HashMap<Trainer, ArrayList> schoolHashMap = createSchoolMap(groupList);
        System.out.println(schoolHashMap.entrySet().toString());
    }

    @Override
    public HashMap<Trainer, ArrayList> createSchoolMap(List<Group> groupList) {
        //The students from different groups are put in an ArrayList first and a HashMap later in this method
        HashMap<Trainer, ArrayList> schoolMapByTrainers = new HashMap<Trainer, ArrayList>();

        for (Group group : groupList) {
            //for each group, creating a List with the students of the group
            List<Student> groupStudents = new ArrayList<>(group.getGroupStudentsList().stream().collect(Collectors.toList()));
            ArrayList<Student> trainerStudentsList;
            for (int i = 0; i < groupStudents.size(); i++) {
                //if the trainer in this group has already has been input in the schoolMapByTrainers,
                //we will recall the ArrayList of the students affiliated to him and add this new student to the cue
                //otherwise each new input in the map with this trainer as a key will erase the previous values
                if (schoolMapByTrainers.containsKey(group.getTrainer())){
                    trainerStudentsList = schoolMapByTrainers.get(group.getTrainer());
                    trainerStudentsList.add(groupStudents.get(i));
                    schoolMapByTrainers.put(group.getTrainer(), trainerStudentsList);
                // Otherwise a new ArrayList of students is created to be affiliated to this trainer
                }else{
                    trainerStudentsList = new ArrayList<Student>();
                    trainerStudentsList.add(groupStudents.get(i));
                    schoolMapByTrainers.put(group.getTrainer(), trainerStudentsList);
                }
            }
        }return schoolMapByTrainers;
    }

    @Override
    public void displayGroupStudentsAlphabetically(List<Student> studentList) {
        //Using the Comparator Interface and lambdas to create a sorting method
        //Students are sorted alphabetically using their last name
        Comparator<Student> compareLastName = (Student s1, Student s2) ->
                s1.getLastName().compareToIgnoreCase(s2.getLastName());
        //Sorting the collection using the comparator
        Collections.sort(studentList, compareLastName);
        //We display all the students last names
        System.out.println(studentList.stream().map(Person::getLastName).collect(Collectors.toList()));
    }

    @Override
    public void displayMaxStudentsGroup(List<Group> groupList) {
        //Using the Comparator Interface to create a sorting method
        //Groups are sorted per numberOfStudents (I use the class parameter created for this purpose)
        Comparator<Group> groupByNumberOfStudents = Comparator.comparing(Group::getNumberOfStudents).reversed();
        groupList.sort(groupByNumberOfStudents);

        System.out.println("Displaying group(s) with most students");
        for (int j = 0; j < 1;) {
            for (int i = 0; i < groupList.size(); i++) {
                Set<Student> studentsNames = groupList.get(i).getGroupStudentsList();
                System.out.println("\n");
                System.out.println(Arrays.asList(groupList.get(i).getGroupName()));
                System.out.println(Arrays.asList(studentsNames.toString()));
                //if the next group has the same number of students it will be displayed too
                if(i < groupList.size()-1) {
                    if (groupList.get(i).getNumberOfStudents() != groupList.get(i + 1).getNumberOfStudents()) {
                        j++;
                    }
                    }else {
                    j++;
                }
            }
        }
    }

    @Override
    public void displayUnder25Students(List<Student> studentList) {
        System.out.println("Displaying students under the age of 25");
        for (Student element : studentList){
            if (element.getAge() < 25){
                System.out.println(element.toString());
            }
        }
    }

    @Override
    public void displayStudentsWithJavaKnowledge(List<Student> studentList) {
        System.out.println("Displaying students with Java knowledge");
        for (Student element : studentList){
            if (element.isHasPreviousJavaKnowledge()){
                System.out.println(element.toString());
            }
        }
    }

    @Override
    public void displayGroupMaxNoJavaKnowledge(List<Group> groupList) {
        //Creating a comparator to sort the groups according to the number of students with JAVA knowledge
        Comparator<Group> groupByNumberOfStudentsWithNoJavaKnowledge = Comparator.comparing(Group::getNumberOfStudentsWithNoJavaKnowledge).reversed();

        groupList.sort(groupByNumberOfStudentsWithNoJavaKnowledge);

        System.out.println("Displaying group(s) with most students with no Java knowledge");
        int j = 0;
        for (int i = 0; j < 1;) {
            //reaching the list of students in each group.
            Set<Student> studentsNames = groupList.get(i).getGroupStudentsList();
            System.out.println("\n");
            //printing the group name
            System.out.println(Arrays.asList(groupList.get(i).getGroupName()));
            //printing the students names
            System.out.println(Arrays.asList(studentsNames.toString()));
            if (groupList.get(i).getNumberOfStudentsWithNoJavaKnowledge() != groupList.get(i+1).getNumberOfStudentsWithNoJavaKnowledge()){
                j++;
            }
            i++;
        }
    }

    @Override
    public void removeUnder20Students(List<Group> groupList) {
        for (Group group : groupList){
            for (Student student : group.getGroupStudentsList()){
                if(student.getAge() < 20){
                    try {
                        group.removeStudentFromGroup(student);
                        break;
                    } catch (Exceptions.StudentNotInGroup s) {
                        s.printStackTrace();
                    }
                }
            }
        }
        System.out.println("Displaying group(s) population");
        for (int i = 0; i < groupList.size();i++) {
            Set<Student> studentsNames = groupList.get(i).getGroupStudentsList();
            System.out.println("\n");
            System.out.println(Arrays.asList(groupList.get(i).getGroupName()));
            System.out.println(Arrays.asList(studentsNames.toString()));
        }
    }

}


