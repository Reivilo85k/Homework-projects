public class Exceptions {

    public static class MaximumNumberOfStudentsReached extends Exception{
        public MaximumNumberOfStudentsReached(String errorMessage) {
            super(errorMessage);
        }
    }

    public static class StudentNotInGroup extends Exception{
        public StudentNotInGroup (String errorMessage) {
            super(errorMessage);
        }
    }

    public static class IllegalCharacterType extends Exception{
        public IllegalCharacterType (String errorMessage) {
            super(errorMessage);
        }
    }

}
