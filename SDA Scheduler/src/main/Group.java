import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Group {

        private  String groupName;
        private Trainer trainer;
        private Set<Student> groupStudentsList;
        private int numberOfStudents = 0;
        private int numberOfGroups = 0;
        private int numberOfStudentsWithJavaKnowledge = 0;

        //created different constructors for different number of students
        //Another solution would have been to have an collection of students as class parameters directly
    public Group(Trainer trainer, Student student1, String groupName) {
        this.trainer = trainer;
        //The students are added in an HashSet. HashSet does not accept duplicates
        this.groupStudentsList = new HashSet<Student>(Arrays.asList(student1));
        //counter following the number of created students
        this.numberOfStudents++;
        //counter following the number of created groups
        this.numberOfGroups++;
        this.groupName = groupName;
        //keeping score of how many students with no JAVA knowledge are in the group
        this.numberOfStudentsWithJavaKnowledge = calculateNumberOfStudentsWithNoJavaKnowledge(groupStudentsList);
    }


    public Group(Trainer trainer, Student student1, Student student2, String groupName) {
            this.trainer = trainer;
            this.groupStudentsList = new HashSet<Student>(Arrays.asList(student1, student2));
            this.numberOfStudents += 2;
            this.numberOfGroups++;
            this.groupName = groupName;
            this.numberOfStudentsWithJavaKnowledge = calculateNumberOfStudentsWithNoJavaKnowledge(groupStudentsList);
    }

        public Group(Trainer trainer, Student student1, Student student2, Student student3, String groupName) {
            this.trainer = trainer;
            this.groupStudentsList = new HashSet<Student>(Arrays.asList(student1, student2, student3));
            this.numberOfStudents += 3;
            this.numberOfGroups++;
            this.groupName = groupName;
            this.numberOfStudentsWithJavaKnowledge = calculateNumberOfStudentsWithNoJavaKnowledge(groupStudentsList);
        }

        public Group(Trainer trainer, Student student1, Student student2, Student student3, Student student4, String groupName) {
            this.trainer = trainer;
            this.groupStudentsList = new HashSet<Student>(Arrays.asList(student1, student2, student3, student4));
            this.numberOfStudents += 4;
            this.numberOfGroups++;
            this.groupName = groupName;
            this.numberOfStudentsWithJavaKnowledge = calculateNumberOfStudentsWithNoJavaKnowledge(groupStudentsList);
        }

        public Group(Trainer trainer, Student student1, Student student2, Student student3, Student student4, Student student5, String groupName) {
            this.trainer = trainer;
            this.groupStudentsList = new HashSet<Student>(Arrays.asList(student1, student2, student3, student4, student5));
            this.numberOfStudents += 5;
            this.numberOfGroups++;
            this.groupName = groupName;
            this.numberOfStudentsWithJavaKnowledge = calculateNumberOfStudentsWithNoJavaKnowledge(groupStudentsList);
        }

        public Group() {
        //An empty group still has to be named and is counted
            this.numberOfGroups++;
            this.groupName = groupName;
        }

        public Trainer getTrainer() {
            return trainer;
        }

        public void setTrainer(Trainer trainer) {
            this.trainer = trainer;
        }

        public Set<Student> getGroupStudentsList() {
            return groupStudentsList;
        }

        public void setGroupStudentsList(Set<Student> groupStudentsList) {
            this.groupStudentsList = groupStudentsList;
        }

        public void setNumberOfStudents(int numberOfStudents) {
        this.numberOfStudents = numberOfStudents;
        }

        public int getNumberOfStudents() {
            return numberOfStudents;
        }

        public String getGroupName() {
        return groupName;
    }

        public int getNumberOfStudentsWithNoJavaKnowledge() {
        //this getter calculate the number of students with no Java knowledge
        //from class parameter numberOfStudentsWithJavaKnowledge
        return numberOfStudents - numberOfStudentsWithJavaKnowledge;
    }

    @Override
    public String toString() {
        return  "\nGroup :" + getGroupName() +
                "\ntrainer : " + trainer.getFirstName() + trainer.getLastName() +
                "\nnumber of students :" +getNumberOfStudents() + " students";
    }

    public int calculateNumberOfStudentsWithNoJavaKnowledge(Set<Student> groupStudentsList){
        int counter = 0;
        for (Student element : groupStudentsList){
           if (element.isHasPreviousJavaKnowledge()){
               counter++;
           }
        }
        return counter;
    }

    public void addStudentInGroup(Student student) throws Exceptions.MaximumNumberOfStudentsReached {
            if (groupStudentsList.size() < 5) {
                groupStudentsList.add(student);
                numberOfStudents++;
            }else{
                //Groups cannot have more than 5 students
                //An exception is thrown if you try to exceed this limit
                throw new Exceptions.MaximumNumberOfStudentsReached("Cannot add student to selected group : the group is already full");
            }
        }

        public void removeStudentFromGroup(Student student) throws Exceptions.StudentNotInGroup {
            if (groupStudentsList.contains(student)) {
                groupStudentsList.remove(student);
                numberOfStudents--;
            }else{
                //An exception is thrown if you try to remove a student who is not in the group
                throw new Exceptions.StudentNotInGroup("Cannot remove student from selected group : the student is not in the group");
            }
        }

        public void changeGroupTrainer(Trainer trainer) throws Exceptions.MaximumNumberOfStudentsReached {
        //method to change a group Trainer. Not implemented in Menu in his version
            setTrainer(trainer);
            System.out.println(trainer.getFirstName() + " " + trainer.getLastName() + " has been assigned to the group");
        }
}
