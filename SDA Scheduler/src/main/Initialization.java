import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Initialization {
    public static School automaticInitialization(){
        List<Student> studentList = automaticStudentListCreation() ;
        List<Trainer> trainerList = automaticTrainerListCreation();
        List<Group> groupList = automaticGroupListCreation(studentList, trainerList);

        School school = new School(studentList, trainerList, groupList);

        return school;
    }

    public static List automaticStudentListCreation(){
        Student student1 = new Student("Ridley", "Scott", LocalDate.of(1937, 11, 30), true);
        Student student2 = new Student("Quentin", "Tarantino", LocalDate.of(1963, 3, 27), true);
        Student student3 = new Student("Denis", "Villeneuve", LocalDate.of(2010, 10, 3), true);
        Student student4 = new Student("Christopher", "Nolan", LocalDate.of(1970, 7, 30), true);
        Student student5 = new Student("George", "Lucas", LocalDate.of(1944, 5, 14), false);
        Student student6 = new Student("Steven", "Spielberg", LocalDate.of(2005, 12, 18), false);
        Student student7 = new Student("David", "Fincher", LocalDate.of(1962, 8, 28), false);
        Student student8 = new Student("Woody", "Allen", LocalDate.of(1935, 12, 1), false);
        Student student9 = new Student("Stanley", "Kubrick", LocalDate.of(2014, 7, 28), false);
        Student student10 = new Student("Francis", "Ford Coppola", LocalDate.of(1939, 4, 7), false);
        Student student11 = new Student("Martin", "Scorsese", LocalDate.of(1986, 5, 4), false);
        Student student12 = new Student("James", "Cameron", LocalDate.of(1954, 8, 16), true);
        Student student13 = new Student("Clint", "Eastwood", LocalDate.of(1930, 5, 31), false);
        Student student14 = new Student("Joel", "Cohen", LocalDate.of(1954, 11, 29), false);
        Student student15 = new Student("Peter", "Jackson", LocalDate.of(1961, 10, 31), false);
        List<Student> studentList = new ArrayList<Student>();
        studentList.add(student1);
        studentList.add(student2);
        studentList.add(student3);
        studentList.add(student4);
        studentList.add(student5);
        studentList.add(student6);
        studentList.add(student7);
        studentList.add(student8);
        studentList.add(student9);
        studentList.add(student10);
        studentList.add(student11);
        studentList.add(student12);
        studentList.add(student13);
        studentList.add(student14);
        studentList.add(student15);
        return studentList;
    }

    public static List automaticTrainerListCreation() {
        Trainer trainer1 = new Trainer("Alfred", "Hitchcock", LocalDate.of(1899, 8, 13), true);
        Trainer trainer2 = new Trainer("Charlie", "Chaplin", LocalDate.of(1889, 4, 16), true);
        Trainer trainer3 = new Trainer("Akira", "Kurosawa", LocalDate.of(1986, 5, 25), true);
        List<Trainer> trainerList = new ArrayList<Trainer>();
        trainerList.add(trainer1);
        trainerList.add(trainer2);
        trainerList.add(trainer3);
        return trainerList;
    }
    public static List automaticGroupListCreation(List<Student> studentList, List<Trainer> trainerList) {
        Group group1 = new Group(trainerList.get(0), studentList.get(0), studentList.get(1), studentList.get(2), studentList.get(3), "Group 1");
        Group group2 = new Group(trainerList.get(1), studentList.get(4),studentList.get(5),studentList.get(6),studentList.get(7), "Group 2");
        Group group3 = new Group(trainerList.get(2), studentList.get(8),studentList.get(9),studentList.get(10),studentList.get(11), "Group 3");
        Group group4 = new Group(trainerList.get(1), studentList.get(12),studentList.get(13),studentList.get(14), "Group 4");
        List<Group> groupList = new ArrayList<Group>();
        groupList.add(group1);
        groupList.add(group2);
        groupList.add(group3);
        groupList.add(group4);
        return groupList;
    }

    public static School manualInitializing(){
        List<Student> studentList = new ArrayList<Student>(15);
        List<Trainer> trainerList = new ArrayList<Trainer>(3);
        List<Group> groupList = new ArrayList<Group>();
        School school = new School();
        studentList = school.createStudentList();
        trainerList = school.createTrainerList();
        groupList = school.createGroupList(studentList, trainerList);
        school.setStudentList(studentList);
        school.setTrainerList(trainerList);
        school.setGroupList(groupList);
        System.out.println("Manual initialization complete");
        return school;
    }
}
