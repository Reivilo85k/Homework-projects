import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public interface ClassManagementInterface {

    List<Student> createStudentList ();
    List<Trainer> createTrainerList ();
    List<Group> createGroupList (List<Student> studentList, List<Trainer> trainerList);
    HashMap<Trainer, ArrayList> createSchoolMap (List<Group> groupList);
    void displayStudentsGroupedByTrainers(List<Group> groupList);
    void displayGroupStudentsAlphabetically (List<Student> studentList);
    void displayMaxStudentsGroup (List<Group> groupList);
    void displayUnder25Students (List<Student> studentList);
    void displayStudentsWithJavaKnowledge (List<Student> studentList);
    void displayGroupMaxNoJavaKnowledge (List<Group> groupList);
    void removeUnder20Students (List<Group> groupList) throws Exceptions.StudentNotInGroup;

}
