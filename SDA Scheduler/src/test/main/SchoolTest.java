import org.junit.Test;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

import static  org.junit.Assert.*;

public class SchoolTest {

    @Test
    public void testCreatePersonList() {
        Student student1 = new Student("straw", "man0", LocalDate.of(1986, 5, 25), true);
        Student student2 = new Student("straw", "man1", LocalDate.of(1986, 6, 25), true);
        Student student3 = new Student("straw", "man2", LocalDate.of(1986, 7, 25), true);
        Student student4 = new Student("straw", "man3", LocalDate.of(1986, 8, 25), true);
        Student student5 = new Student("straw", "man4", LocalDate.of(1986, 9, 25), true);
        Student student6 = new Student("straw", "man5", LocalDate.of(1986, 10, 25), true);
        Student student7 = new Student("straw", "man6", LocalDate.of(1986, 11, 25), true);
        Student student8 = new Student("straw", "man7", LocalDate.of(1986, 12, 25), true);
        Student student9 = new Student("straw", "man8", LocalDate.of(1986, 5, 2), true);
        Student student10 = new Student("straw", "man9", LocalDate.of(1986, 5, 3), true);
        Student student11 = new Student("straw", "man10", LocalDate.of(1986, 5, 4), true);
        Student student12 = new Student("straw", "man11", LocalDate.of(1986, 5, 5), true);
        Student student13 = new Student("straw", "man12", LocalDate.of(1986, 5, 8), true);
        Student student14 = new Student("straw", "man13", LocalDate.of(1986, 5, 9), true);
        Student student15 = new Student("straw", "man14", LocalDate.of(1986, 5, 12), true);

        List<Student> helperList = new ArrayList<Student>();
        helperList.add(0, student1);
        helperList.add(1, student2);
        helperList.add(2, student3);
        helperList.add(3, student4);
        helperList.add(4, student5);
        helperList.add(5, student6);
        helperList.add(6, student7);
        helperList.add(7, student8);
        helperList.add(8, student9);
        helperList.add(9, student10);
        helperList.add(10, student11);
        helperList.add(11, student12);
        helperList.add(12, student13);
        helperList.add(13, student14);
        helperList.add(14, student15);


        List<Student> studentList = new ArrayList<Student>(15);
        int index = 0;
        int i = 0;

        while (index < 15) {
            Student student = new Student();
            studentList.add(index, helperList.get(i));
            index++;
            i++;
        }

        assertEquals(helperList, studentList);

    }

    @Test
    public void testCreateGroupList() {
        Student student1 = new Student("straw", "man0", LocalDate.of(1986, 5, 25), true);
        Student student2 = new Student("straw", "man1", LocalDate.of(1986, 6, 25), true);
        Student student3 = new Student("straw", "man2", LocalDate.of(1986, 7, 25), true);
        Student student4 = new Student("straw", "man3", LocalDate.of(1986, 8, 25), true);
        Student student5 = new Student("straw", "man4", LocalDate.of(1986, 9, 25), true);
        Student student6 = new Student("straw", "man5", LocalDate.of(1986, 10, 25), true);
        Student student7 = new Student("straw", "man6", LocalDate.of(1986, 11, 25), true);
        Student student8 = new Student("straw", "man7", LocalDate.of(1986, 12, 25), true);
        Student student9 = new Student("straw", "man8", LocalDate.of(1986, 5, 2), true);
        Student student10 = new Student("straw", "man9", LocalDate.of(1986, 5, 3), true);
        Student student11 = new Student("straw", "man10", LocalDate.of(1986, 5, 4), true);
        Student student12 = new Student("straw", "man11", LocalDate.of(1986, 5, 5), true);
        Student student13 = new Student("straw", "man12", LocalDate.of(1986, 5, 8), true);
        Student student14 = new Student("straw", "man13", LocalDate.of(1986, 5, 9), true);
        Student student15 = new Student("straw", "man14", LocalDate.of(1986, 5, 12), true);

        List<Student> helperList = new ArrayList<Student>();
        helperList.add(0, student1);
        helperList.add(1, student2);
        helperList.add(2, student3);
        helperList.add(3, student4);
        helperList.add(4, student5);
        helperList.add(5, student6);
        helperList.add(6, student7);
        helperList.add(7, student8);
        helperList.add(8, student9);
        helperList.add(9, student10);
        helperList.add(10, student11);
        helperList.add(11, student12);
        helperList.add(12, student13);
        helperList.add(13, student14);
        helperList.add(14, student15);

        LocalDate dateOfBirth = LocalDate.of(1986, 5, 25);
        Trainer trainer1 = new Trainer("straw", "man0", LocalDate.of(1986, 5, 25), true);
        Trainer trainer2 = new Trainer("straw", "man1", LocalDate.of(1986, 6, 25), true);
        Trainer trainer3 = new Trainer("straw", "man2", LocalDate.of(1986, 7, 25), true);

        List<Trainer> helperList2 = new ArrayList<Trainer>();
        helperList2.add(0, trainer1);
        helperList2.add(1, trainer2);
        helperList2.add(2, trainer3);

        School school = new School();
        school.setStudentList(helperList);
        school.setTrainerList(helperList2);

        int studentIndex = 0;
        int trainerIndex = 0;
        int groupListIndex = 0;
        int numberOfGroups = 0;

        List<Group> groupListResult = new ArrayList<Group>();

        while (studentIndex < 15) {
            String groupName = "Group" + numberOfGroups;
            Group newGroup = new Group(helperList2.get(trainerIndex),
                    helperList.get(studentIndex),
                    helperList.get(studentIndex + 1),
                    helperList.get(studentIndex + 2), groupName);

            groupListResult.add(groupListIndex, newGroup);

            studentIndex += 3;
            if (trainerIndex < 2) {
                trainerIndex++;
            } else trainerIndex = ThreadLocalRandom.current().nextInt(0, 2 + 1);
        }

        System.out.println(groupListResult.stream().collect(Collectors.toList()));
        groupListResult.stream().forEach(s -> System.out.println(s.toString()));
    }

    @Test
    public void testTrainerIndexFunction() {

        int result = ThreadLocalRandom.current().nextInt(0, 2 + 1);
        System.out.println(result);
        assert (result <= 2);

    }

    @Test
    public void testAddStudentInGroupSuccess() throws Exceptions.MaximumNumberOfStudentsReached {
        Trainer trainer = new Trainer("straw", "man0", LocalDate.of(1986, 5, 25), true);
        Student student1 = new Student("straw", "man0", LocalDate.of(1986, 5, 25), true);
        Student student2 = new Student("straw", "man1", LocalDate.of(1986, 6, 25), true);
        Student student3 = new Student("straw", "man2", LocalDate.of(1986, 7, 25), true);
        Group result = new Group(trainer, student1, student2, student3, "Group result");

        Student student4 = new Student("straw", "man3", LocalDate.of(1986, 8, 25), true);
        Group expected = new Group(trainer, student1, student2, student3, student4, "group expected");

        result.addStudentInGroup(student4);

        assertEquals(expected.getGroupStudentsList(), result.getGroupStudentsList());
        assertEquals(expected.getTrainer(), result.getTrainer());
    }

    @Test(expected = Exceptions.MaximumNumberOfStudentsReached.class)
    public void testAddStudentInGroupFailure() throws Exceptions.MaximumNumberOfStudentsReached {
        Trainer trainer = new Trainer("straw", "man0", LocalDate.of(1986, 5, 25), true);
        Student student1 = new Student("straw", "man0", LocalDate.of(1986, 5, 25), true);
        Student student2 = new Student("straw", "man1", LocalDate.of(1986, 6, 25), true);
        Student student3 = new Student("straw", "man2", LocalDate.of(1986, 7, 25), true);
        Student student4 = new Student("straw", "man3", LocalDate.of(1986, 8, 25), true);
        Student student5 = new Student("straw", "man4", LocalDate.of(1986, 9, 25), true);

        Group result = new Group(trainer, student1, student2, student3, student4, student5, "group result");

        Student student6 = new Student("straw", "man5", LocalDate.of(1986, 10, 25), true);
        Group expected = new Group(trainer, student1, student2, student3, student4, student5, "group expected");

        result.addStudentInGroup(student6);

        assertEquals(expected.getGroupStudentsList(), result.getGroupStudentsList());
        assertEquals(expected.getTrainer(), result.getTrainer());


    }

    @Test
    public void testRemoveStudentInGroupSuccess() throws Exceptions.MaximumNumberOfStudentsReached, Exceptions.StudentNotInGroup {
        Trainer trainer = new Trainer("straw", "man0", LocalDate.of(1986, 5, 25), true);
        Student student1 = new Student("straw", "man0", LocalDate.of(1986, 5, 25), true);
        Student student2 = new Student("straw", "man1", LocalDate.of(1986, 6, 25), true);
        Student student3 = new Student("straw", "man2", LocalDate.of(1986, 7, 25), true);
        Group result = new Group(trainer, student1, student2, student3, "group result");

        Student student4 = new Student("straw", "man3", LocalDate.of(1986, 8, 25), true);
        Group expected = new Group(trainer, student1, student2, student3, "group expected");

        result.addStudentInGroup(student4);
        result.removeStudentFromGroup(student4);

        assertEquals(expected.getGroupStudentsList(), result.getGroupStudentsList());
        assertEquals(expected.getTrainer(), result.getTrainer());
    }

    @Test(expected = Exceptions.StudentNotInGroup.class)
    public void testRemoveStudentInGroupFailure() throws Exceptions.StudentNotInGroup {
        Trainer trainer = new Trainer("straw", "man0", LocalDate.of(1986, 5, 25), true);
        Student student1 = new Student("straw", "man0", LocalDate.of(1986, 5, 25), true);
        Student student2 = new Student("straw", "man1", LocalDate.of(1986, 6, 25), true);
        Student student3 = new Student("straw", "man2", LocalDate.of(1986, 7, 25), true);
        Student student4 = new Student("straw", "man3", LocalDate.of(1986, 8, 25), true);
        Student student5 = new Student("straw", "man4", LocalDate.of(1986, 9, 25), true);

        Group result = new Group(trainer, student1, student2, student3, student4, student5, "group result");

        Student student6 = new Student("straw", "man5", LocalDate.of(1986, 10, 25), true);
        Group expected = new Group(trainer, student1, student2, student3, student4, student5, "group expected");

        result.removeStudentFromGroup(student6);

        assertEquals(expected.getGroupStudentsList(), result.getGroupStudentsList());
        assertEquals(expected.getTrainer(), result.getTrainer());
    }

    @Test
    public void testDisplayGroupStudentsAlphabetically() {
        Trainer trainer = new Trainer("straw", "man0", LocalDate.of(1986, 5, 25), true);

        Student student1 = new Student("straw", "Abe", LocalDate.of(1986, 5, 12), true);
        Student student2 = new Student("straw", "Babe", LocalDate.of(1986, 5, 12), true);
        Student student3 = new Student("straw", "Cabe", LocalDate.of(1986, 5, 12), true);
        Student student4 = new Student("straw", "Dabe", LocalDate.of(1986, 5, 12), true);
        Student student5 = new Student("straw", "Ebe", LocalDate.of(1986, 5, 4), true);
        Student student6 = new Student("straw", "Febe", LocalDate.of(1986, 5, 4), true);
        Student student7 = new Student("straw", "Gebe", LocalDate.of(1986, 5, 4), true);
        Student student8 = new Student("straw", "Hebe", LocalDate.of(1986, 5, 4), true);
        Student student9 = new Student("straw", "Ilebe", LocalDate.of(1986, 5, 4), true);
        Student student10 = new Student("straw", "Jebe", LocalDate.of(1986, 5, 4), true);
        Student student11 = new Student("straw", "Kebe", LocalDate.of(1986, 5, 4), true);
        Student student12 = new Student("straw", "Tebe", LocalDate.of(1986, 8, 25), true);
        Student student13 = new Student("straw", "Sebe", LocalDate.of(1986, 7, 25), true);
        Student student14 = new Student("straw", "Webe", LocalDate.of(1986, 6, 25), true);
        Student student15 = new Student("straw", "Zebe", LocalDate.of(1986, 5, 25), true);

        List<Student> expected = new LinkedList<Student>();
        expected.add(student1);
        expected.add(student2);
        expected.add(student3);
        expected.add(student4);
        expected.add(student5);
        expected.add(student6);
        expected.add(student7);
        expected.add(student8);
        expected.add(student9);
        expected.add(student10);
        expected.add(student11);
        expected.add(student12);
        expected.add(student13);
        expected.add(student14);
        expected.add(student15);

        Student student01 = new Student("straw", "Zebe", LocalDate.of(1986, 5, 25), true);
        Student student02 = new Student("straw", "Webe", LocalDate.of(1986, 6, 25), true);
        Student student03 = new Student("straw", "Sebe", LocalDate.of(1986, 7, 25), true);
        Student student04 = new Student("straw", "Tebe", LocalDate.of(1986, 8, 25), true);
        Student student05 = new Student("straw", "Kebe", LocalDate.of(1986, 9, 25), true);
        Student student06 = new Student("straw", "Jebe", LocalDate.of(1986, 10, 25), true);
        Student student07 = new Student("straw", "Ilebe", LocalDate.of(1986, 11, 25), true);
        Student student08 = new Student("straw", "Hebe", LocalDate.of(1986, 12, 25), true);
        Student student09 = new Student("straw", "Gebe", LocalDate.of(1986, 5, 2), true);
        Student student010 = new Student("straw", "Febe", LocalDate.of(1986, 5, 3), true);
        Student student011 = new Student("straw", "Ebe", LocalDate.of(1986, 5, 4), true);
        Student student012 = new Student("straw", "Dabe", LocalDate.of(1986, 5, 5), true);
        Student student013 = new Student("straw", "Cabe", LocalDate.of(1986, 5, 8), true);
        Student student014 = new Student("straw", "Babe", LocalDate.of(1986, 5, 9), true);
        Student student015 = new Student("straw", "Abe", LocalDate.of(1986, 5, 12), true);
        List<Student> actual = new LinkedList<Student>();
        actual.add(student01);
        actual.add(student02);
        actual.add(student03);
        actual.add(student04);
        actual.add(student05);
        actual.add(student06);
        actual.add(student07);
        actual.add(student08);
        actual.add(student09);
        actual.add(student010);
        actual.add(student011);
        actual.add(student012);
        actual.add(student013);
        actual.add(student014);
        actual.add(student015);

        School school = new School();
        System.out.println(expected.stream().map(Person::getLastName).collect(Collectors.toList()));
        school.displayGroupStudentsAlphabetically(actual);
    }

    @Test
    public void testDisplayMaxStudentsGroup() {
        Student student1 = new Student("straw", "Abe", LocalDate.of(1986, 5, 12), true);
        Student student2 = new Student("straw", "Babe", LocalDate.of(1986, 5, 12), true);
        Student student3 = new Student("straw", "Cabe", LocalDate.of(2016, 5, 12), true);
        Student student4 = new Student("straw", "Dabe", LocalDate.of(1993, 5, 12), true);
        Student student5 = new Student("straw", "Ebe", LocalDate.of(1950, 5, 4), true);
        Student student6 = new Student("straw", "Febe", LocalDate.of(2005, 5, 4), true);

        List<Student> studentList = new ArrayList<Student>();
        studentList.add(student1);
        studentList.add(student2);
        studentList.add(student3);
        studentList.add(student4);
        studentList.add(student5);
        studentList.add(student6);


        Trainer trainer1 = new Trainer("straw", "man0", LocalDate.of(1986, 5, 25), true);
        List<Trainer> trainerList = new ArrayList<Trainer>();
        trainerList.add(trainer1);

        Group group1 = new Group(trainer1, student1, "Group 1");
        Group group2 = new Group(trainer1, student2, student3, "Group 2");
        Group group3 = new Group(trainer1, student4, student5, student6, "Group 3");
        List<Group> groupList = new ArrayList<Group>();
        groupList.add(group2);
        groupList.add(group1);
        groupList.add(group3);

        School school = new School(studentList, trainerList, groupList);
        school.displayUnder25Students(school.getStudentList());

    }

    @Test
    public void testDisplayStudentsWithNoJavaKnowledge() {
        Student student1 = new Student("straw", "Abe", LocalDate.of(1986, 5, 12), false);
        Student student2 = new Student("straw", "Babe", LocalDate.of(1986, 5, 12), false);
        Student student3 = new Student("straw", "Cabe", LocalDate.of(2016, 5, 12), false);
        Student student4 = new Student("straw", "Dabe", LocalDate.of(1993, 5, 12), true);
        Student student5 = new Student("straw", "Ebe", LocalDate.of(1950, 5, 4), false);
        Student student6 = new Student("straw", "Febe", LocalDate.of(2005, 5, 4), true);

        List<Student> studentList = new ArrayList<Student>();
        studentList.add(student1);
        studentList.add(student2);
        studentList.add(student3);
        studentList.add(student4);
        studentList.add(student5);
        studentList.add(student6);


        Trainer trainer1 = new Trainer("straw", "man0", LocalDate.of(1986, 5, 25), true);
        List<Trainer> trainerList = new ArrayList<Trainer>();
        trainerList.add(trainer1);

        Group group1 = new Group(trainer1, student1, "Group 1");
        Group group2 = new Group(trainer1, student2, student3, "Group 2");
        Group group3 = new Group(trainer1, student4, student5, student6, "Group 3");
        List<Group> groupList = new ArrayList<Group>();
        groupList.add(group2);
        groupList.add(group1);
        groupList.add(group3);

        School school = new School(studentList, trainerList, groupList);
        school.displayGroupMaxNoJavaKnowledge(school.getGroupList());
    }

    @Test
    public void testRemoveUnder20Students() {
        Student student1 = new Student("straw", "Abe", LocalDate.of(2016, 5, 12), true);
        Student student2 = new Student("straw", "Babe", LocalDate.of(1986, 5, 12), true);
        Student student3 = new Student("straw", "Cabe", LocalDate.of(2001, 5, 12), true);
        Student student4 = new Student("straw", "Dabe", LocalDate.of(1998, 5, 12), true);
        Student student5 = new Student("straw", "Ebe", LocalDate.of(1950, 5, 4), true);
        Student student6 = new Student("straw", "Febe", LocalDate.of(2005, 5, 4), true);

        List<Student> studentList = new ArrayList<Student>();
        studentList.add(student1);
        studentList.add(student2);
        studentList.add(student3);
        studentList.add(student4);
        studentList.add(student5);
        studentList.add(student6);


        Trainer trainer1 = new Trainer("straw", "man0", LocalDate.of(1986, 5, 25), true);
        List<Trainer> trainerList = new ArrayList<Trainer>();
        trainerList.add(trainer1);

        Group group1 = new Group(trainer1, student1, "Group 1");
        Group group2 = new Group(trainer1, student2, student3, "Group 2");
        Group group3 = new Group(trainer1, student4, student5, student6, "Group 3");
        List<Group> groupList = new ArrayList<Group>();
        groupList.add(group2);
        groupList.add(group1);
        groupList.add(group3);

        School school = new School(studentList, trainerList, groupList);
        school.removeUnder20Students(school.getGroupList());
    }

    @Test
    public void testDisplayStudentsGroupedByTrainers() {
        Student student1 = new Student("straw", "Abe", LocalDate.of(2016, 5, 12), true);
        Student student2 = new Student("straw", "Babe", LocalDate.of(1986, 5, 12), true);
        Student student3 = new Student("straw", "Cabe", LocalDate.of(2001, 5, 12), true);
        Student student4 = new Student("straw", "Dabe", LocalDate.of(1998, 5, 12), true);
        Student student5 = new Student("straw", "Ebe", LocalDate.of(1950, 5, 4), true);
        Student student6 = new Student("straw", "Febe", LocalDate.of(2005, 5, 4), true);

        List<Student> studentList = new ArrayList<Student>();
        studentList.add(student1);
        studentList.add(student2);
        studentList.add(student3);
        studentList.add(student4);
        studentList.add(student5);
        studentList.add(student6);


        Trainer trainer1 = new Trainer("straw", "man0", LocalDate.of(1986, 5, 25), true);
        Trainer trainer2 = new Trainer("stra", "man1", LocalDate.of(1976, 4, 5), true);
        List<Trainer> trainerList = new ArrayList<Trainer>();
        trainerList.add(trainer1);

        Group group1 = new Group(trainer1, student1, "Group 1");
        Group group2 = new Group(trainer2, student2, student3, "Group 2");
        Group group3 = new Group(trainer2, student4, student5, student6, "Group 3");
        List<Group> groupList = new ArrayList<Group>();
        groupList.add(group2);
        groupList.add(group1);
        groupList.add(group3);

        School school = new School(studentList, trainerList, groupList);
        school.displayStudentsGroupedByTrainers(school.getGroupList());

    }
}

